# Add information to commits about which issues, pull requests and other
# commits reference them.
#
# This helps find duplicate bug reports, and follow up commits when backporting
# commits for LTS releases.
#
# It uses the git notes feature to attach notes to commits, which we then push
# to the repository and Gitea display will display them.

import config
import utils

import re
from typing import Sequence

PATTERN_TOKENS = re.compile(r" |\n|\[\]|:|\.|\?|\!|\{|\}")
PATTERN_SHA1 = re.compile(r"[0-9a-f]{7,40}")

# From the bug reporting template.
IGNORE_COMMITS = ["edbf15d3c044"]
MAX_MENTIONS = 100

def find_references(repo: str, msg: str):
    for token in re.split(PATTERN_TOKENS, msg):
        token_repo = repo

        # Parse repo prefix.
        if "@" in token:
            repo_commit = token.split("@")
            if len(repo_commit) == 2:
                if repo_commit[0] in config.GITEA_REPOS:
                    token_repo = repo_commit[0]
                    token = repo_commit[1]
        # Parse old phabricator references.
        elif token.startswith("rBA"):
            token = token[3:]
        elif token.startswith("rB"):
            token = token[2:]

        # Check if token is valid sha1 and exists in the repo.
        if re.fullmatch(PATTERN_SHA1, token) and \
           token not in IGNORE_COMMITS and \
           utils.git_is_commit(token_repo, token):
            yield token_repo, token

def update_commit_note(repo: str, commit: str, new_line: str):
    note = utils.git_get_note(repo, commit)
    lines = note.split('\n')

    new_line = "Referenced by " + new_line

    # Don't generate an excessive amount of references, this may happen for
    # commits that are mentioned in a bug reporting template for example.
    if len(lines) > MAX_MENTIONS:
        return False
    # Don't add the same line twice. This means we can safely re-run the
    # update on issues if the webhook was down for some reason.
    elif new_line in lines:
        return False
    elif len(lines) == MAX_MENTIONS:
        new_line = "Maximum number of references exceeded"

    lines.append(new_line)
    note = "\n".join(lines)
    utils.git_set_note(repo, commit, note)
    return True

def update_commit_notes(references: Sequence, fetch_and_push: bool=True):
    if len(references) == 0:
        return

    fetch_repos = set()
    push_repos = set()
    for commit_repo, commit, line in references:
        if fetch_and_push and commit_repo not in fetch_repos:
            utils.git_fetch_notes(commit_repo)
            fetch_repos.add(commit_repo)
        if update_commit_note(commit_repo, commit, line):
            if fetch_and_push:
                push_repos.add(commit_repo)

    for changed_repo in push_repos:
        utils.git_push_notes(changed_repo)

def get_references_in_issue_or_pr(repo: str, issue_or_pr: str, prefix: str) -> Sequence:
    # PR does not give API url, so manually construct.
    issue_number = str(issue_or_pr["number"])
    issue_url = config.GITEA_API_URL + "/repos/" + repo + "/issues/" + issue_number
    issue_title = issue_or_pr["title"]

    # Find all commits references in comments of this issue.
    comments = [issue_or_pr["body"]]
    for comment in utils.gitea_api_get(issue_url + "/comments"):
        comments.append(comment["body"])

    references = []
    for comment in comments:
        for commit_repo, commit in find_references(repo, comment):
            issue_ref = "#" + issue_number
            if repo != commit_repo:
                issue_ref = repo + issue_ref
            line = prefix + " " + issue_ref + ", " + issue_title
            references.append((commit_repo, commit, line))

    return references

def issues_pulls_event(data, fetch_and_push: bool=True):
    repo = data["repository"]["full_name"]
    if repo not in config.GITEA_REPOS:
        return

    # Pull request events can use "issue" somtimes but then set "is_pull".
    if "pull_request" in data:
        issue_or_pr = data["pull_request"]
        prefix = "pull request"
    else:
        issue_or_pr = data["issue"]
        prefix = "issue"
    if data.get("is_pull", False):
        prefix = "pull request"

    # Add notes to commits.
    references = get_references_in_issue_or_pr(repo, issue_or_pr, prefix)
    update_commit_notes(references, fetch_and_push=fetch_and_push)

def get_references_in_commit(repo: str, commit_id: str, message: str) -> Sequence:
    references = []
    for commit_repo, commit in find_references(repo, message):
        push_ref = str(commit_id)
        if repo != commit_repo:
            push_ref = repo + "@" + line
        push_title = message.split("\n")[0]
        line = "commit " + push_ref + ", " + push_title
        references.append((commit_repo, commit, line))
    return references

def push_event(data, fetch_and_push: bool=True):
    repo = data["repository"]["full_name"]
    if repo not in config.GITEA_REPOS:
        return

    # Only take into account main and release branches.
    ref = data["ref"]
    if not ((ref in {"refs/heads/master", "refs/heads/main"}) or \
            (ref.startswith("ref/heads/blender-v") and ref.endswith("-release"))):
        return

    # Find all commits references in the pushed commits.
    references = []
    for push_commit in data["commits"]:
        references += get_references_in_commit(repo, push_commit["id"], push_commit["message"])

    # Add notes to commits.
    update_commit_notes(references, fetch_and_push=fetch_and_push)

# Batch updates
def batch_update_issues(repo: str, max_updates: int=0):
    num = utils.gitea_api_get_total_count(config.GITEA_API_URL + "/repos/" + repo + "/issues?state=all&limit=1")
    if max_updates != 0:
        num = min(num, max_updates)
    limit = 10
    num_pages = (num + limit - 1) // limit

    for page in range(0, num_pages + 1):
        issues = utils.gitea_api_get(config.GITEA_API_URL + "/repos/" + repo + "/issues" + f"?state=all&limit={limit}&page={page}")
        for i, issue in enumerate(reversed(issues)):
            issue_number = issue["number"]

            print(f"{repo} issue #{issue_number} {(num_pages - page)*limit + i}/{num}")
            references = get_references_in_issue_or_pr(repo, issue, "issue")
            update_commit_notes(references, fetch_and_push=False)

def batch_update_commits(repo: str, max_updates: int=0):
    commit_ids = utils.git_fetch_all_commits_in_branch(repo, "main")
    if max_updates != 0:
        commit_ids = commit_ids[:max_updates]
    num = len(commit_ids)

    for i, commit_id in enumerate(reversed(commit_ids)):
        commit_id = commit_ids[i]
        print(f"{repo} commit {commit_id} {i}/{num}")

        message = utils.git_get_commit_message(repo, commit_id)
        references = get_references_in_commit(repo, commit_id, message)
        update_commit_notes(references, fetch_and_push=False)
