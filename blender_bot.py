#!/usr/bin/python3

import config
import commands
import labels
import notes
import traceback
import utils

from flask import Flask, request, abort
app = Flask(__name__)

def handle_request(event, data):
    if event == "issues":
        labels.issues_event(data)
        notes.issues_pulls_event(data)
    elif event == "issue_comment":
        notes.issues_pulls_event(data)
        commands.comment_event(data)
    elif event == "pull_request":
        notes.issues_pulls_event(data)
        commands.pull_request_event(data)
    elif event == "pull_request_comment":
        notes.issues_pulls_event(data)
        commands.comment_event(data)
    elif event == "push":
        notes.push_event(data)

@app.route("/webhook", methods=["POST"])
def root():
    # Get event and data
    event = request.headers.get("X-Gitea-Event", "")
    try:
        data = request.get_json()
    except Exception:
        utils.log("Failed to parse json from request")
        abort(400)

    # Handle
    try:
        handle_request(event, data)
    except Exception as err:
        traceback.print_exc()
        utils.log("Failed to handle request: " + str(err))
        abort(400)

    return "OK"

if __name__ == "__main__":
    app.run(port=config.DEBUG_WEBHOOK_PORT)
